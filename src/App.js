import { useState } from 'react';
import './App.css';
import Form from './components/Form';
import List from './components/List';
import Score from './components/Score';



function App() { 
  const [bmi, setBmi] = useState("");
  const [type, setxtype] = useState("Not calculated");
  const [bmilist, setbmilist] = useState({
    underweight : {low:""},
    overweight : {low:"", high : ""},
    normal : {low:"", high : ""}
  }  
  );

  const [looseweight, setlooseweight] = useState({weight: "", type: ""})
  const [show, setShow] = useState(false)

  const changeweight = (w, bmi, range)=>{
    let obj;
    if(bmi > 24.9){
      obj = {
        weight: (w - range.normal.high).toFixed(2),
        type: "positive"
      };
      return obj;
    }
    else if(bmi < 18.5){
      obj = {
        weight: (range.normal.high - w).toFixed(2),
        type: "negative"
      };
      return obj;
    }
    else{
      obj = {
        weight: 0,
        type: "normal"
      };
      return obj;

    }

  }

  const onFormSub = (w,h) =>{
    console.log(w,h)
    let data = calcBmi(w,h);
    setBmi(data)
    setxtype(weighttype(data))

    const range = {
      underweight : {low:calcweight(18.5, h)},
      normal : {low:calcweight(18.6, h), high : calcweight(24.9, h)},
      overweight : {low:calcweight(25, h), high : calcweight(29.9, h)}
    }
    setbmilist(range)
    setlooseweight(changeweight(w, data, range));
    setShow(true);
    
  };

  const calcBmi = (w,h) => (w/(h*h)).toFixed(2);
  const calcweight = (data, h) => (data * h * h).toFixed(2);

  const weighttype = (bmi) =>{
    if (bmi < 18.5){
      return "Under weight";
    }
    else if (18.5<bmi && bmi < 24.9){
      return "Normal";
    }
    else if (24.5< bmi){
      return "Overweight";
    }
  };

  return (
    <>
    <Form getData={onFormSub}/>
    ({show &&
      <div className='container'>
        <div className='row'>
          <div className='col-sm-4'>
            <Score weight={bmi} type={type} looseweight={looseweight}/>
          </div>
          <div className='col-sm-8'>
            <List bmirange={bmilist} bmi={bmi}/>
          </div>
        </div>
      </div>
    })
    
    </>
  );
}

export default App;
