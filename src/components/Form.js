import { useState } from "react";

function Form({getData}) {
    const [height, setheight] = useState("")
    const [weight, setweight] = useState("")
    const [alert, setalert] = useState(false)


    const onSubmit = (e) =>{
        e.preventDefault();
        
        if(isNaN(weight) || isNaN(height)){
            console.log('not a valid number')
            setalert(true)
        }
        else{
            getData(weight, height)
            setalert(false)
            setheight("")
            setweight("")
        }

    };

    // let alertmessage;
    // if (alert){
    //     alertmessage = <div className="alert alert-danger" role="alert"> enter valid data</div>
    // }
    // else{
    //     alertmessage= "";
    // }
    

  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="col-sm-4 shadow rounded p-5 m-5">
          <h1 className="text-center pt-3 text-secondary h2">
            Weight Management System
          </h1>
          <form onSubmit={onSubmit}>
            <div className="row">
              <div className="col-12 col-md-6">
                <div className="my-3">
                  <label className="form-label">Weight</label>
                  <input type="text" className="form-control" id="weightid" aria-describedby="weight" value={weight} onChange={(e) =>setweight(e.target.value)} required/>
                </div>
              </div>
              <div className="col-12 col-md-6">
                <div className="my-3">
                  <label className="form-label">height</label>
                  <input type="text" className="form-control" id="height" value={height} onChange={(e) =>setheight(e.target.value)} required />
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12 text-center">
                <input type="submit" className="btn-primary my-3" vlaue="Get" />
              </div>
            </div>
          </form>
          {alert &&<div className="alert alert-danger" role="alert"> enter valid data</div>}
        </div>
      </div>
    </div>
  );
}

export default Form;
