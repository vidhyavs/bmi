
function Score({weight, type, looseweight}) {
    console.log(looseweight)
    return (
        // <div className="container">
        //     <div className="row">
        //     <div className="col-6">
                <div className="text-center shadow rounded p-4">
                    <div> Your weight analysis</div>
                    <div className="row justify-content-md-center">
                        <div className="p-3 my-2 alert fs-2 alert-primary col-sm-4">{weight}</div>
                    </div>
                    <div className="fs-3 fw-bold text-primary">{type}</div>
                    {
                    looseweight.type === "positive" && (
                            <div className="fs-4"> "You need to lose <span className="fw-bold">{looseweight.weight} kg</span></div>
                    )}
                    {
                    looseweight.type === "negative" && (
                        <div className="fs-4"> "You need to gain <span className="fw-bold">{looseweight.weight} kg</span></div>
                    )}
                    {
                    looseweight.type === "normal" && (
                        <div className="fs-4"> "Your weight is normal, Good for you </div>
                    )}
                </div>
        //     </div>
        //     </div>
        // </div>
        
    )
}

export default Score
