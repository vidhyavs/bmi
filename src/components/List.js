
function List({bmirange, bmi}) {
    console.log({bmirange})
    console.log({bmi})
    return (
        <div className="text-center shadow rounded p-4">
            <table className="table">
                <thead>
                    <tr>
                    
                    <th scope="col">Type</th>
                    <th scope="col">Bmi (kg)</th>
                    <th scope="col">Weight (kg)</th>
                    </tr>
                </thead>
                <tbody>
                    <tr></tr>
                    <tr className="border border-3">
                    <td>Under weight</td>
                    <td>&lt; 18.5 </td>
                    <td className={ bmi < 18.5 ? "text-danger":""}>&lt; 
                        {bmirange.underweight.low}
                    </td>
                    </tr>
                    <tr></tr>
                    <tr className="border border-3">
                    <td>Normal</td>
                    <td>18.6 - 24.9</td>
                    <td className={18.6 < bmi && bmi < 24.9 ? "text-success":""}>
                        {bmirange.normal.low + "-" + bmirange.normal.high}
                    </td>
                    </tr>
                    <tr></tr>
                    <tr className="border border-3">                    
                    <td>Overweight</td>
                    <td>25 - 29.9</td>
                    <td className={25 < bmi ? "text-danger":""}>
                        {bmirange.overweight.low + "-" + bmirange.overweight.high}
                    </td>
                    </tr>
                </tbody>
            </table>
        </div>
    )
}

export default List
